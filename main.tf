resource "azurerm_resource_group" "create_rg" {
  location = "${var.location}"
  name = "${var.rg_name}"

}


resource "azurerm_virtual_network" "vnet" {
  address_space = ["${var.address_space}"]
  location = "${var.location}"
  name = "${var.network_name}"
  resource_group_name = "${var.rg_name}"
}


resource "azurerm_subnet" "subnet" {
  address_prefix = "${var.subnet_prefixes[count.index]}"
  name = "${var.subnet_names[count.index]}"
  resource_group_name = "${var.rg_name}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  count = "${length(var.subnet_names)}"
}
















