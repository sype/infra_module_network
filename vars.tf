
variable "network_name" {}
variable "name" {}
variable "location" {}
variable "rg_name" {}
variable "subnet_id" {}
variable "rg_prefix" {
  default = "test"
}

variable "address_space" {
  description = "The network adress for the main network"
  type = "list"
  default = ["10.0.1.0/24"]
}


variable "subnet_names" {
  description = "A list of public subnets inside the vNet."
  type = "list"
  default     = ["public"]
}


variable "subnet_prefixes" {
  description = "List of prefix to use with subnet"
  type = "list"
  default = ["10.0.1.0/24"]
}



variable "tags" {
  description = "The tags to associate with your network and subnets."
  type        = "map"

  default = {
    env = "dev"
    app = "infra"
  }
}