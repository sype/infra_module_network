# terraform_vnet



## Create a basic virtual network in Azure

This Terraform module deploys a Virtual Network in Azure with a subnet or a set of subnets passed in as input parameters.

The module does not create nor expose a security group. This would need to be defined separately as additional security rules on subnets in the deployed network.

## Usage

```hcl
module "vnet" {
    source              = "Anetwork"
    resource_group_name = "resourcename"
    location            = "westus"
    address_space       = "10.0.0.0/16"
    subnet_prefixes     = ["10.0.1.0/24"]
    subnet_names        = ["subnet1"]

    tags                = {
                            env = "dev"
                            app  = "it"
                          }
}

```

## Example to create a subnet only

```hcl

terraform apply -target module.network.azurerm_subnet.subnet

```
